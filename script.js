function filterBy(arr, dataType) {
    return arr.filter((item) => typeof item !== dataType);
  }
  
  const data = ["Ukraine", 253, null, [], {}, false, undefined, function () {}];
  const filteredData = filterBy(data, "string");
  console.log(filteredData);